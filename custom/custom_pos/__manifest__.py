{
    'name': 'Custom POS',
    'version': '1.0',
    'summary': 'Summery',
    'description': 'Description',
    'category': 'Category',
    'author': 'Author',
    'website': 'Website',
    'depends': ['point_of_sale'],
    'licence': 'LGPL-3',
    'data': [
            'xml/view.xml'
    ],
    'assets': {
        'point_of_sale.assets': [
            'custom_pos/static/src/js/sample_button.js',
            'custom_pos/static/src/xml/sample_button.xml',
            'custom_pos/static/src/js/custom_numpad.js',
            'custom_pos/static/src/xml/custom_numpad.xml',
        ],
    },
    'installable': True,
    'auto_install': False
}
