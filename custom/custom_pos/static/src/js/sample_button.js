odoo.define('custom_pos.CustomSampleButton', function (require) {
    'use strict';

    const PosComponent = require('point_of_sale.PosComponent');
    const ProductScreen = require('point_of_sale.ProductScreen');
    const Registries = require('point_of_sale.Registries');
    const { useListener } = require('@web/core/utils/hooks')

    const rpc = require('web.rpc')

    class SampleButton extends PosComponent{

        setup(){
            super.setup();
            useListener('click', this.sample_button_click)
        }

        sample_button_click(){
            var res = rpc.query({
                model: 'hospital.patient',
                method: 'search_read',
                args: [[], ['name', 'age']]
            }).then(function(products){
                console.log(products)
            })
        }

    }

    SampleButton.template = 'CustomSampleButton';
    ProductScreen.addControlButton({
        component: SampleButton,
        position: ['before', 'OrderlineCustomerNoteButton']
    });

    Registries.Component.add(SampleButton);

    return SampleButton;
});
