from odoo import fields, models, api, _


class ClassModel(models.Model):
    _name = 'school.class'
    _description = 'Lớp học'
    _inherit = ["mail.thread", "mail.activity.mixin"]

    reference = fields.Char(string="Mã Lớp", required=True, copy=False, readOnly=True,
                            default=lambda self: _('New'))
    name = fields.Char(string="Tên Lớp", required=True)
    student_ids = fields.One2many('school.student', 'class_id', string='Danh Sách Học Sinh')
    student_count = fields.Integer(string='Sĩ Số', compute='_compute_student_count')

    @api.model
    def create(self, vals):
        if vals.get('reference', _('New') == _('New')):
            vals['reference'] = self.env['ir.sequence'].next_by_code('school.class') or _('New')
        res = super(ClassModel, self).create(vals)
        return res

    def _compute_student_count(self):
        student_count = self.env['school.student'].search_count([('class_id', '=', self.id)])
        self.student_count = student_count

    def name_get(self):
        result = []
        for rec in self:
            if not self.env.context.get('hide_code'):
                name = f'[{rec.reference}] {rec.name}'
            else:
                name = rec.name
            result.append((rec.id, name))
        return result

    def action_open_students(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Danh sách sinh viên',
            'res_model': 'school.student',
            'domain': [('class_id', '=', self.id)],
            'context': {'default_class_id': self.id},
            'view_mode': 'tree,form',
            'target': 'current'
        }


