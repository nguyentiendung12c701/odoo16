from odoo import fields, models, api, _
from datetime import datetime
from odoo.exceptions import ValidationError


class StudentModel(models.Model):
    _name = 'school.student'
    _description = 'School Student'
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _order = 'reference asc'

    @api.model
    def default_get(self, fields):
        res = super(StudentModel, self).default_get(fields)
        res['name'] = 'Nhập họ tên học sinh'
        res['date_of_birth'] = datetime(2001, 1, 1)
        return res

    name = fields.Char(string='Tên học sinh', required=True, tracking=True)
    reference = fields.Char(string="Mã học sinh", required=True, copy=False, readOnly=True,
                            default=lambda self: _('New'))
    responsible_id = fields.Many2one('res.partner', string="Phụ Huynh")
    date_of_birth = fields.Date(string='Ngày sinh', tracking=True)
    age = fields.Integer(string='Tuổi', compute='_compute_age')
    gender = fields.Selection([
        ('male', 'Nam'),
        ('female', 'Nữ'),
        ('other', 'Khác')
    ], string='Giới tính', required=True, default='male', tracking=True)
    image = fields.Binary(string='Ảnh đại diện')
    state = fields.Selection([
        ('studied', 'Đang học'),
        ('suspended', 'Đình chỉ'),
        ('graduated', 'Tốt nghiệp')
    ], default='studied', string='Trạng thái', tracking=True)
    class_id = fields.Many2one('school.class', string='Lớp Học', required=True)

    @api.model
    def create(self, vals):
        if vals.get('reference', _('New') == _('New')):
            vals['reference'] = self.env['ir.sequence'].next_by_code('school.student') or _('New')
        res = super(StudentModel, self).create(vals)
        return res

    @api.depends('date_of_birth')
    def _compute_age(self):
        for record in self:
            if record.date_of_birth:
                record.age = datetime.now().year - record.date_of_birth.year
            else:
                record.age = 0

    def action_suspended(self):
        self.state = 'suspended'

    def action_graduated(self):
        self.state = 'graduated'

    def action_cancel(self):
        self.state = 'studied'

    @api.constrains('name')
    def check_name(self):
        for rec in self:
            if not rec.date_of_birth:
                raise ValidationError(_('Không được để trống ngày sinh'))

    def unlink(self):
        if self.state == 'studied':
            raise ValidationError(_("Không được xóa học sinh %s khi vẫn còn đang học" % self.name))
        return super(StudentModel, self).unlink()
