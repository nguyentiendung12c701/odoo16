# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class CreateStudentWizard(models.TransientModel):
    _name = 'create.student.wizard'
    _description = 'Create Student Wizard'

    @api.model
    def default_get(self, fields):
        res = super(CreateStudentWizard, self).default_get(fields)
        res['class_id'] = self._context.get('active_id')
        return res

    name = fields.Char(string='Tên học sinh', tracking=True)
    date_of_birth = fields.Date(string='Ngày sinh', tracking=True)
    gender = fields.Selection([
        ('male', 'Nam'),
        ('female', 'Nữ'),
        ('other', 'Khác')
    ], string='Giới tính', default='male', tracking=True)
    image = fields.Binary(string='Ảnh đại diện')
    class_id = fields.Many2one('school.class', string='Lớp', required=True)

    def action_create_student(self):
        vals = {
            'class_id': self.class_id.id,
            'name': self.name,
            'date_of_birth': self.date_of_birth,
            'gender': self.gender,
            'image': self.image
        }
        student_rec = self.env['school.student'].create(vals)
        return {
            'name': _('Student'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'school.student',
            'res_id': student_rec.id,
            'target': 'new'
        }

    def action_view_student(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Student',
            'res_model': 'school.student',
            'view_type': 'form',
            'domain': [('class_id', '=', self.class_id.id)],
            'view_mode': 'tree,form',
            'target': 'current'
        }
