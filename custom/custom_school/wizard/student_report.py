from odoo import fields, models, api, _


class StudentReportWizard(models.TransientModel):
    _name = 'student.report.wizard'
    _description = 'Print Student Wizard'

    class_id = fields.Many2one('school.class', string='Lớp học')
    state = fields.Selection([
        ('studied', 'Đang học'),
        ('suspended', 'Đình chỉ'),
        ('graduated', 'Tốt nghiệp')
    ], default='studied', string='Trạng thái')

    def action_print_excel_report(self):
        domain = []

        class_id = self.class_id
        if class_id:
            domain += [('class_id', '=', class_id.id)]

        state = self.state
        if state:
            domain += [('state', '=', state)]

        students = self.env['school.student'].search_read(domain)
        data = {
            'form_data': self.read()[0],
            'students': students
        }

        return self.env.ref('custom_school.report_student_xlsx').report_action(self, data=data)

    def action_print_report(self):
        domain = []

        class_id = self.class_id
        if class_id:
            domain += [('class_id', '=', class_id.id)]

        state = self.state
        if state:
            domain += [('state', '==', state)]

        students = self.env['school.student'].search_read(domain)

        data = {
            'form_data': self.read()[0],
            'students': students
        }
        return self.env.ref('custom_school.action_report_print_student').report_action(self, data=data)
