{
    'name': 'Quản Lý Trường Học',
    'version': '1.0',
    'summary': 'School Management Software',
    'description': 'School Management Software',
    'category': 'Productivity',
    'author': 'Author',
    'website': 'Website',
    'license': 'LGPL-3',
    'depends': [
        'mail',
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'wizard/create_student_view.xml',
        'wizard/student_report_view.xml',
        'views/gender_view.xml',
        'views/student_view.xml',
        'views/class_view.xml',
        'views/menu.xml',
        'report/report.xml',
    ],
    'demo': [],
    'installable': True,
    'application': True,
    'auto_install': False
}
