# -*- coding: utf-8 -*-
import base64
import io

from odoo import api, fields, models, _


class AgeStudentXlsx(models.AbstractModel):
    _name = 'report.custom_school.report_student_xls'
    _description = 'Print Student'
    _inherit = 'report.report_xlsx.abstract'

    def generate_xlsx_report(self, workbook, data, patients):
        bold = workbook.add_format({'bold': True})

        sheet = workbook.add_worksheet('Học Sinh')
        row = 1
        col = 1

        sheet.set_column('G:G', 30)
        sheet.set_column('H:H', 5)
        sheet.set_column('I:I', 10)
        sheet.set_column('J:J', 20)

        sheet.write(row, col, 'Họ tên', bold)
        sheet.write(row, col + 1, 'Tuổi', bold)
        sheet.write(row, col + 2, 'Giới tính', bold)
        sheet.write(row, col + 3, 'Ngày sinh', bold)

        for student in data['students']:
            print(student)
            row += 1
            sheet.write(row, col, student['name'])
            sheet.write(row, col + 1, student['age'])
            result = 'Nam' if student['gender'] == 'male' else 'Nữ' if student['gender'] == 'female' else 'Khác'
            sheet.write(row, col + 2, result)
            sheet.write(row, col + 3, student['date_of_birth'])

