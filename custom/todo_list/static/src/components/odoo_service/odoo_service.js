/** @odoo-module */

import { registry } from "@web/core/registry"
import { Layout } from "@web/search/layout"
import { getDefaultConfig } from "@web/views/view"
import { useService } from "@web/core/utils/hooks"
import { ConfirmationDialog } from "@web/core/confirmation_dialog/confirmation_dialog"
import { routeToUrl } from "@web/core/browser/router_service"
import { browser } from "@web/core/browser/browser"

const { Component, useSubEnv, useState } = owl

class OwlOdooService extends Component {
    setup(){
        this.display = {
            controlPanel: {"top-right": false, "bottom-right": false}
        }

        useSubEnv({
            config: {
                ...getDefaultConfig(),
                ...this.env.config,
            }
        })

        this.cookieService = useService("cookie")

        if (this.cookieService.current.dark_theme == undefined){
            this.cookieService.setCookie("dark_theme", false)
        }

        const router = this.env.services.router

        this.state = useState({
            dark_theme: this.cookieService.current.dark_theme,
            get_http_data: [],
            post_http_data: [],
            rpc_data: [],
            orm_data: [],
            bg_success: router.current.search.bg_success,
            user_data: null,
            company_data: null,
        })

//        const titleService = useService("title")
//        titleService.setParts({zopenerp: "Test", odoo: "test", any:"..."})
//        console.log(titleService.getParts())
    }

    showNotification(){
        const notification = this.env.services.notification
        notification.add('test noti...', {
            title: 'Odoo notification service',
            type: 'danger',
            sticky: true,
            className: 'pb-4',
            buttons: [
                {
                  name: 'Goto Google',
                  onClick: ()=>{
                    window.location.href = 'https://www.google.com';
                  },
                  primary: true
                },
                {
                  name: 'Show me again',
                  onClick: ()=>{
                    this.showNotification()
                  },
                  primary: false
                },
            ]
        })
    }

    dialogService(){
        const dialog = this.env.services.dialog
        dialog.add(ConfirmationDialog, {
                title: 'Dialog Service',
                body: 'This is dialog service message',
                confirm: () => {
                    alert('dialog confirm')
                },
                cancel: () => {
                    alert('dialog cancel')
                }
            },{
                onClose: () => {
                alert('dialog close')
                }
            }
        )
    }

    effectService(){
        const effect = this.env.services.effect
        effect.add({
            type: 'rainbow_man',
            message: 'This is effect message',
        })
    }

    setCookieService(){
        if (this.cookieService.current.dark_theme == 'false'){
            this.cookieService.setCookie("dark_theme", true)
        } else {
            this.cookieService.setCookie("dark_theme", false)
        }

        this.state.dark_theme = this.cookieService.current.dark_theme

        this.cookieService.deleteCookie("test")
    }

    async getHttpService(){
        const http = this.env.services.http
        const data = await http.get('https://dummyjson.com/products')
        console.log(data)
        this.state.get_http_data = data.products
    }

    async postHttpService(){
        const http = this.env.services.http
        const data = await http.post('https://dummyjson.com/products/add', {title: 'asfasf',})
        console.log(data)
        this.state.post_http_data = data
    }

    async getRpcService(){
        const rpc = this.env.services.rpc
        const data = await rpc("/owl/rpc_service", {limit: 10})
        console.log(data)
        this.state.rpc_data = data
    }

    async getOrmService(){
        const orm = this.env.services.orm
        const data = await orm.searchRead('res.partner', [], ['name', 'email'])
        console.log(data)
        this.state.orm_data = data
    }

    getActionService(){
        const action = this.env.services.action
        action.doAction({
            type: "ir.actions.act_window",
            name: "Action Service",
            res_model: "res.partner",
            domain:[],
            context:{search_default_type_company: 1},
            views:[
                [false, "list"],
                [false, "form"],
                [false, "kanban"],
            ],
            view_mode:"list,form,kanban",
            target: "current"
        })
    }

    getRouterService(){
        const router = this.env.services.router
        let { search } = router.current
        search.bg_success = search.bg_success == "1" ? "0" : "1"
        browser.location.href = browser.location.origin + routeToUrl(router.current)
    }

    getUserService(){
        const user = this.env.services.user
        console.log(user)
        this.state.user_data = JSON.stringify(user)
    }

    getCompanyService(){
        const company = this.env.services.company
        console.log(company)
        this.state.company_data = JSON.stringify(company)
    }
}

OwlOdooService.template = 'owl.OdooServices'
OwlOdooService.components = { Layout }

registry.category("actions").add("owl.OdooServices", OwlOdooService)