/** @odoo-module */

import { registry } from "@web/core/registry"
import { Layout } from "@web/search/layout"
import { getDefaultConfig } from "@web/views/view"
import { useService } from "@web/core/utils/hooks"
import { ConfirmationDialog } from "@web/core/confirmation_dialog/confirmation_dialog"
import { routeToUrl } from "@web/core/browser/router_service"
import { browser } from "@web/core/browser/browser"

const { Component, useSubEnv, useState } = owl

class OwlOdooService extends Component {
    setup(){
        this.display = {
            controlPanel: {"top-right": false, "bottom-right": false}
        }

    }
}

OwlOdooService.template = 'owl.Dashboard'
OwlOdooService.components = { Layout }

registry.category("actions").add("owl.Dashboard", OwlOdooService)