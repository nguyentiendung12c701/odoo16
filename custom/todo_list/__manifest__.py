# -*- coding: utf-8 -*-
{
    'name': "Todo List App",

    'summary': """
        Todo List App""",
    'sequence': -1,
    'description': """
        Todo List App
    """,

    'author': "My Company",
    'website': "https://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/16.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/todo_list_view.xml',
        'views/res_partner.xml',
        'views/odoo_service.xml',
    ],

    'assets': {
        'web.assets_backend': [
            'todo_list/static/src/components/*/*.js',
            'todo_list/static/src/components/*/*.xml',
            'todo_list/static/src/components/*/*.scss',
        ],
    },

    'application': True,
    'installable': True,
}
