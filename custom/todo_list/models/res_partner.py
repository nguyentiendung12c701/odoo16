# -*- coding: utf-8 -*-

from odoo import fields, models, api

class ResPartner(models.Model):
    _inherit = 'res.partner'

    username = fields.Char()
    expected_salary = fields.Integer()
