import random

from odoo import http, _
from odoo.http import route, request
from odoo.addons.portal.controllers.portal import CustomerPortal, pager
from odoo.tools import groupby as groupbyelem
from operator import itemgetter


class OdooHospital(http.Controller):
    @route(['/new/patient'], auth='user', methods=["POST", "GET"], type='http', website=True)
    def display_register(self, **kw):
        responsible_id = request.env['res.partner'].search([])
        vals = {
            'page_name': 'patient_register',
            'responsible_id': responsible_id
        }
        if request.httprequest.method == "POST":
            error_list = []
            if not kw.get("name"):
                error_list.append('Name field is mandatory')
            if not kw.get("age"):
                error_list.append('Age field is mandatory')
            if not request.env['res.partner'].search([('id', '=', kw.get('responsible_id'))]):
                error_list.append('Invalid responsible field selected value.')
            if not error_list:
                request.env['hospital.patient'].create({'name': kw['name'],
                                                        'age': kw['age'],
                                                        'gender': kw['gender'],
                                                        'responsible_id': kw['responsible_id'],
                                                        'note': kw['note']})
                vals['success_msg'] = 'A new patient has been successfully registered!'
            else:
                vals['error_list'] = error_list

        return request.render('om_hospital.new_patient_form_view_portal', vals)

    @route(['/patient', '/patient/page/<int:page>'], auth='user', type='http', website=True)
    def display_subject(self, page=1, sortby='name', groupby='none', search='', search_in='name'):
        patient_domain = ('state', 'in', ['draft', 'confirm', 'done'])

        sort_list = {
            'name': {'label': _('Name'), 'order': 'name'},
            'date': {'label': _('Newest'), 'order': 'create_date desc'},
            'age': {'label': _('Age'), 'order': 'age'},
        }

        groupby_list = {
            'none': {'input': 'none', 'label': _('None'), 'order': 1},
            'age': {'input': 'age', 'label': _('Age'), 'order': 1},
            'gender': {'input': 'gender', 'label': _('Gender'), 'order': 1},
        }

        search_list = {
            'name': {'label': _('Name'), 'input': 'name', 'domain': [patient_domain, ('name', 'ilike', search)]},
            'gender': {'label': _('Gender'), 'input': 'gender', 'domain': [patient_domain, ('gender', 'ilike', search)]},
            'responsible': {'label': _('Responsible'), 'input': 'responsible',
                            'domain': [patient_domain, ('responsible_id.name', 'ilike', search)]},
        }

        patient_group_by = groupby_list.get(groupby, {})
        search_domain = search_list[search_in]['domain']
        order = sort_list[sortby]['order']
        groupby_keys = (key for key in groupby_list.keys() if key != 'none')
        if groupby in groupby_keys:
            patient_group_by = patient_group_by.get('input')
            order = f'{patient_group_by}, {order}'
        else:
            patient_group_by = ''
        patient_obj = request.env['hospital.patient']
        total_patient = patient_obj.search_count(search_domain)
        stud_url = '/patient'
        page_detail = pager(url= stud_url,
                            total=total_patient,
                            page=page,
                            url_args={'sortby': sortby, 'groupby': groupby, 'search_in': search_in, 'search': search},
                            step=10)
        patients = patient_obj.search(search_domain, limit=10, offset=page_detail['offset'], order=order)
        if patient_group_by:
            patient_group_list = [{patient_group_by: k, 'patients': patient_obj.concat(*g)} for k, g in groupbyelem(patients, itemgetter(patient_group_by))]
        else:
            patient_group_list = [{'none': 'none', 'patients': patients}]

        vals = {
            # 'patients': patients,
            'patient_group_list': patient_group_list,
            'page_name': 'patient',
            'pager': page_detail,
            'default_url': stud_url,
            'sortby': sortby,
            'searchbar_sortings': sort_list,
            'groupby': groupby,
            'searchbar_groupby': groupby_list,
            'search_in': search_in,
            'searchbar_inputs': search_list,
            'search': search
        }

        return request.render('om_hospital.portal_patients', vals)

    @route('/patient/<model("hospital.patient"):patient>/', auth='user', type='http', website=True)
    def display_detail(self, patient):
        vals = {
            'patient': patient,
            'page_name': 'patient'
        }

        patient_records = request.env['hospital.patient'].search([('state', 'in', ['draft', 'confirm', 'done'])])
        patient_ids = patient_records.ids
        patient_index = patient_ids.index(patient.id)

        if patient_index != 0 and patient_ids[patient_index - 1]:
            vals['prev_record'] = '/patient/{}'.format(patient_ids[patient_index - 1])
        if patient_index < len(patient_ids) and patient_ids[patient_index + 1]:
            vals['next_record'] = '/patient/{}'.format(patient_ids[patient_index + 1])

        return request.render('om_hospital.patients_detail', vals)

class HospitalPatientPortal(CustomerPortal):

    def _prepare_home_portal_values(self, counters):
        values = super()._prepare_home_portal_values(counters)
        patient = request.env['hospital.patient']
        if 'patient_count' in counters:
            values['patient_count'] = patient.search_count([('state', 'in', ['draft', 'confirm', 'done'])])
        if 'patient_register' in counters:
            values['patient_register'] = 'New'

        return values

    @route('/patient/download/<model("hospital.patient"):patient>/', auth='user', type='http', website=True)
    def download_patient(self, patient):
        return self._show_report(model=patient, report_type='pdf',
                                 report_ref='om_hospital.report_patient_detail',
                                 download=True)
