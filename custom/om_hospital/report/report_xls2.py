# -*- coding: utf-8 -*-
import base64
import io

from odoo import api, fields, models, _


class PatientAppointmentXlsx(models.AbstractModel):
    _name = 'report.om_hospital.xls_report_excel'
    _inherit = 'report.report_xlsx.abstract'
    _description = 'report'

    def generate_xlsx_report(self, workbook, data, model):
        center = workbook.add_format({'align': 'center', 'valign': 'vcenter'})
        date = workbook.add_format({'align': 'center', 'valign': 'vcenter', 'border': True})
        vcenter = workbook.add_format({'valign': 'vcenter'})
        header = workbook.add_format({'bold': True, 'font_size': 16, 'align': 'center',
                                      'valign': 'vcenter', 'border': True})

        length = len(data['data']) + 4

        worksheet = workbook.add_worksheet('Báo cáo nhập xuất hàng')

        worksheet.merge_range('A1:E1', 'Báo cáo nhập xuất hàng', header)
        worksheet.merge_range('A2:E2', f"(Từ: {data['from']} - Đến {data['to']})", date)

        worksheet.add_table(f'A3:E{length}', {
            'data': data['data'],
            'style': 'Table Style Light 12',
            'total_row': 1,
            'columns': [
                {'header': 'Ngày',
                 'format': center,
                 'header_format': center,
                 'total_string': 'Tổng:'},
                {'header': 'Sản phẩm',
                 'format': vcenter,
                 'header_format': center},
                {'header': 'Nhập/Xuất',
                 'format': center,
                 'header_format': center,
                 'total_function': 'sum'},
                {'header': 'Số dư đầu kỳ',
                 'format': center,
                 'header_format': center,
                 'total_function': 'sum'},
                {'header': 'Lũy kế',
                 'format': center,
                 'header_format': center,
                 'total_function': 'sum'}
            ]
        })

        worksheet.set_column('A:A', 12)
        worksheet.set_column('B:B', 25)
        worksheet.set_column('C:E', 15)

        for i in range(0, length):
            worksheet.set_row(i, 20)
