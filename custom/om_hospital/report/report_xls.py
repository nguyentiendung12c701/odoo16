# -*- coding: utf-8 -*-
import base64
import io

from odoo import api, fields, models, _


class PatientAppointmentXlsx(models.AbstractModel):
    _name = 'report.om_hospital.report_excel_xls'
    _inherit = 'report.report_xlsx.abstract'
    _description = 'report'

    def generate_xlsx_report(self, workbook, data, model):
        center = workbook.add_format({'align': 'center', 'valign': 'vcenter'})
        vcenter = workbook.add_format({'valign': 'vcenter'})
        title = workbook.add_format({'bold': True, 'font_size': 16, 'align': 'center', 'valign': 'vcenter',
                                     'border': True})

        length = len(data['data']) + 2

        worksheet = workbook.add_worksheet('Báo cáo bán hàng')

        if data['month'] and data['year']:
            worksheet.merge_range('A1:J1', f"Báo cáo bán hàng {data['month']}/{data['year']}", title)
        else:
            worksheet.merge_range('A1:J1', 'Báo cáo bán hàng', title)

        worksheet.add_table(f'A2:J{length}', {
            'data': data['data'],
            'style': 'Table Style Light 12',
            'total_row': 1,
            'columns': [
                {'header': 'Khách hàng',
                 'format': vcenter,
                 'header_format': center},
                {'header': 'Sản phẩm',
                 'format': vcenter,
                 'header_format': center,
                 'total_string': 'Tổng:'},
                {'header': 'Số lượng',
                 'format': center,
                 'header_format': center,
                 'total_function': 'sum'},
                {'header': 'Đã giao',
                 'format': center,
                 'header_format': center,
                 'total_function': 'sum'},
                {'header': 'Trả hàng',
                 'format': center,
                 'header_format': center,
                 'total_function': 'sum'},
                {'header': 'Đã trả',
                 'format': center,
                 'header_format': center,
                 'total_function': 'sum'},
                {'header': 'Giá gốc',
                 'format': center,
                 'header_format': center,
                 'total_function': 'sum'},
                {'header': 'Giá bán',
                 'format': center,
                 'header_format': center,
                 'total_function': 'sum'},
                {'header': 'Lãi/Lỗ',
                 'format': center,
                 'header_format': center,
                 'total_function': 'sum'},
                {'header': 'Ngày',
                 'format': center,
                 'header_format': center}
            ]
        })

        worksheet.set_column('A:A', 18)
        worksheet.set_column('B:B', 25)
        worksheet.set_column('C:J', 15)

        for i in range(0, length):
            worksheet.set_row(i, 20)
        worksheet.set_row(0, 30)
