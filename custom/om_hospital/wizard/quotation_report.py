from odoo import fields, models, api, _
from odoo.exceptions import ValidationError
import datetime


class AppointmentReportWizard(models.TransientModel):
    _name = 'quotation.report.wizard'
    _description = 'Print Quotation Wizard'

    month = fields.Selection([
        ('1', 'January'), ('2', 'February'), ('3', 'March'), ('4', 'April'),
        ('5', 'May'), ('6', 'June'), ('7', 'July'), ('8', 'August'),
        ('9', 'September'), ('10', 'October'), ('11', 'November'), ('12', 'December')
    ], string='Month')

    year = fields.Selection([
        ('2022', '2022'), ('2023', '2023')
    ], string='Year')

    date_from = fields.Date(string="From", required=True)
    date_to = fields.Date(string="To", required=True)

    @api.constrains('date_from')
    def _check_date_from(self):
        for record in self:
            if record.date_from > fields.Date.today():
                raise ValidationError(_(
                    'Date from cannot be set in the future'))

    @api.constrains('date_to')
    def _check_date_to(self):
        for record in self:
            if record.date_to <= record.date_from:
                raise ValidationError(_(
                    'Date to cannot be set in the past'))

    def action_print_quotation_report(self):
        query_quotation = """SELECT
                                sale_order.NAME,
                                res_partner.NAME,
                                res_partner.phone,
                                res_partner.email,
                                res_partner.street,
                                res_partner.city,
                                sale_order.amount_untaxed,
                                sale_order.amount_tax,
                                sale_order.amount_total 
                            FROM
                                sale_order
                                INNER JOIN res_partner ON sale_order.partner_id = res_partner.ID 
                            WHERE
                                sale_order.delivery_status IS NOT NULL 
                            ORDER BY
                                sale_order.NAME"""

        self.env.cr.execute(query_quotation)
        list_quotation = self.env.cr.fetchall()

        list_deli = {}
        list_inv = {}
        list_prod = {}

        for quotation in list_quotation:
            query_prod = f"""SELECT
                                product_template.NAME [ 'en_US' ],
                                sale_order_line.product_uom_qty,
                                sale_order_line.qty_delivered,
                                sale_order_line.qty_invoiced,
                                sale_order_line.price_unit,
                                sale_order_line.price_subtotal,
                                sale_order_line.untaxed_amount_invoiced 
                            FROM
                                sale_order_line
                                INNER JOIN sale_order ON sale_order_line.order_id = sale_order."id"
                                INNER JOIN product_product ON sale_order_line.product_id = product_product."id" 
                                INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                            WHERE
                                sale_order.NAME = '{quotation[0]}'"""
            self.env.cr.execute(query_prod)
            products = self.env.cr.fetchall()
            list_prod[quotation[0]] = products

            query_deli = f"""SELECT
                                stock_picking.NAME,
                                product_template.NAME [ 'en_US' ],
                                stock_move.product_uom_qty,
                                stock_move.quantity_done,
                                stock_picking.STATE
                            FROM
                                stock_picking
                                INNER JOIN stock_move ON stock_picking.ID = stock_move.picking_id
                                INNER JOIN product_product ON stock_move.product_id = product_product.ID 
                                INNER JOIN product_template ON product_product.product_tmpl_id = product_template.ID 
                                INNER JOIN sale_order ON stock_picking.sale_id = sale_order.ID 
                            WHERE 
                                sale_order.name = '{quotation[0]}' 
                            ORDER BY
                                stock_picking.NAME"""
            self.env.cr.execute(query_deli)
            delivered = self.env.cr.fetchall()
            list_deli[quotation[0]] = delivered

            query_inv = f"""SELECT
                                account_move.NAME,
                                account_move.DATE,
                                account_move.amount_untaxed_signed,
                                account_move.amount_total_signed,
                                account_move.payment_state,
                                account_move.STATE 
                            FROM
                                account_move 
                            WHERE
                                account_move.invoice_origin = '{quotation[0]}'"""
            self.env.cr.execute(query_inv)
            inv = self.env.cr.fetchall()
            list_inv[quotation[0]] = inv

        data = {
            'list_quotation': list_quotation,
            'list_deli': list_deli,
            'list_inv': list_inv,
            'list_prod': list_prod
        }

        # for value in list_quotation:
        #     print(data['list_inv'][value[0]])

        return self.env.ref('om_hospital.report_quotation').report_action(self, data=data)

    def action_print_export_report(self):
        e_query = f'''SELECT
                    product_template."name" [ 'en_US' ],
                    stock_move_line.reference,
                    stock_move_line."date",
                    slf.complete_name,
                    slt.complete_name,
                    res_partner."name",
                    stock_move.product_uom_qty,
                    stock_move.quantity_done 
                FROM
                    stock_move_line
                    INNER JOIN product_product ON stock_move_line.product_id = product_product."id"
                    INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                    INNER JOIN stock_location slf ON stock_move_line.location_id = slf."id"
                    INNER JOIN stock_location slt ON stock_move_line.location_dest_id = slt."id"
                    INNER JOIN stock_move ON stock_move_line.move_id = stock_move."id" 
                    INNER JOIN stock_picking ON stock_move_line.picking_id = stock_picking."id"
                    INNER JOIN res_partner ON stock_picking.partner_id = res_partner."id"
                WHERE
                    (slf."usage" = 'internal' OR slf."usage" = 'transit')
                    AND stock_move.quantity_done != 0
                ORDER BY
                    product_template."name"'''

        self.env.cr.execute(e_query)
        export_products = self.env.cr.fetchall()

        i_query = f'''SELECT
                        product_template."name" [ 'en_US' ],
                        stock_move_line.reference,
                        stock_move_line."date",
                        slf.complete_name,
                        slt.complete_name,  
                        stock_move.quantity_done 
                    FROM
                        stock_move_line
                        INNER JOIN product_product ON stock_move_line.product_id = product_product."id"
                        INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                        INNER JOIN stock_location slf ON stock_move_line.location_id = slf."id"
                        INNER JOIN stock_location slt ON stock_move_line.location_dest_id = slt."id"
                        INNER JOIN stock_move ON stock_move_line.move_id = stock_move."id"  
                    WHERE
                        ( slf."usage" != 'internal' AND slf."usage" != 'transit' ) 
	                    AND stock_move.quantity_done != 0 
                    ORDER BY
                        product_template."name" '''

        self.env.cr.execute(i_query)
        import_products = self.env.cr.fetchall()
        data = {
            'export_products': export_products,
            'import_products': import_products
        }

        return self.env.ref('om_hospital.export_report_xlsx').report_action(self, data=data)

    def action_print_month_report(self):
        now = datetime.datetime.now()
        last_12_months = []
        for i in range(1, 13):
            month = now.month - i
            year = now.year
            if month <= 0:
                month += 12
                year -= 1
            key = f'{month}{year}'
            month_year_tuple = (month, year, key)
            last_12_months.append(month_year_tuple)

        e_prod = {}
        i_prod = {}

        for month in last_12_months:
            e_query = f'''SELECT
                            product_template."name" [ 'en_US' ],
                            SUM(stock_move.product_uom_qty),
                            SUM(stock_move.quantity_done)
                        FROM
                            stock_move_line
                            INNER JOIN product_product ON stock_move_line.product_id = product_product."id"
                            INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                            INNER JOIN stock_move ON stock_move_line.move_id = stock_move."id"
                            INNER JOIN stock_location ON stock_move_line.location_id = stock_location."id"
                        WHERE
                            (stock_location."usage" = 'internal' OR stock_location."usage" = 'transit')
                            AND EXTRACT(MONTH FROM stock_move_line."date") = {month[0]}
                            AND EXTRACT(YEAR FROM stock_move_line."date") = {month[1]}
                        GROUP BY
                            product_template."name"
                        ORDER BY
                            product_template."name"'''
            self.env.cr.execute(e_query)
            e_query_data = self.env.cr.fetchall()
            e_prod[month[2]] = e_query_data

            i_query = f'''SELECT
                            product_template."name" [ 'en_US' ],  
                            SUM(stock_move.quantity_done)
                        FROM
                            stock_move_line
                            INNER JOIN product_product ON stock_move_line.product_id = product_product."id"
                            INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                            INNER JOIN stock_move ON stock_move_line.move_id = stock_move."id"
                            INNER JOIN stock_location ON stock_move_line.location_id = stock_location."id" 
                        WHERE
                            (stock_location."usage" != 'internal' AND stock_location."usage" != 'transit') 
                            AND stock_move_line."state" = 'done'
                            AND EXTRACT(MONTH FROM stock_move_line."date") = {month[0]}
                            AND EXTRACT(YEAR FROM stock_move_line."date") = {month[1]}
                        GROUP BY 
                            product_template."name" 
                        ORDER BY
                            product_template."name"'''
            self.env.cr.execute(i_query)
            i_query_data = self.env.cr.fetchall()
            i_prod[month[2]] = i_query_data

        data = {
            'export_products': e_prod,
            'import_products': i_prod,
            'months': last_12_months
        }

        for month in data['months']:
            print(data['import_products'][month[2]])
        return self.env.ref('om_hospital.month_report').report_action(self, data=data)

    def action_print_month_report_new(self):
        now = datetime.datetime.now()
        last_12_months = []
        for i in range(1, 13):
            month = now.month - i
            year = now.year
            if month <= 0:
                month += 12
                year -= 1
            key = f'{month}{year}'
            month_year_tuple = (month, year, key)
            last_12_months.append(month_year_tuple)

        prod = {}

        for month in last_12_months:
            query = f'''SELECT 
                               res_partner."name",
                               res_partner.phone, 
                               res_partner.city, 
                               product_template."name" [ 'en_US' ],
                               stock_move.product_uom_qty,
                               stock_move.quantity_done
                           FROM
                               stock_move_line
                               INNER JOIN stock_move on stock_move_line.move_id = stock_move."id"
                               INNER JOIN product_product ON stock_move_line.product_id = product_product."id"
                               INNER JOIN product_template ON product_product.product_tmpl_id = product_template."id"
                               INNER JOIN stock_location ON stock_move_line.location_id = stock_location."id" 
                               INNER JOIN stock_picking ON stock_move_line.picking_id = stock_picking."id"
                               INNER JOIN res_partner ON stock_picking.partner_id = res_partner."id"	
                           WHERE
                               stock_move_line."state" = 'done' 
                               AND ( stock_location."usage" = 'internal' OR stock_location."usage" = 'transit' ) 
                               AND EXTRACT ( MONTH FROM stock_move_line."date" ) = {month[0]}
                               AND EXTRACT ( YEAR FROM stock_move_line."date" ) = {month[1]}
                           ORDER BY res_partner."name" ASC'''
            self.env.cr.execute(query)
            query_data = self.env.cr.fetchall()
            prod[month[2]] = query_data

        data = {
            'products': prod,
            'months': last_12_months
        }

        # for month in data['months']:
        #     print(month, data['products'][month[2]])
        return self.env.ref('om_hospital.new_month_report').report_action(self, data=data)

    def print_excel_action(self):
        query = f"""WITH return_products AS (
                        SELECT SUBSTRING
                            ( sp.origin, 11 ) AS reference,
                            sml.product_id,
                            SUM ( sm.product_uom_qty ) AS return_demand,
                            SUM ( sm.quantity_done ) AS returned 
                        FROM
                            stock_move_line sml
                            LEFT JOIN stock_move sm ON sml.move_id = sm."id"
                            LEFT JOIN stock_location sl ON sml.location_id = sl."id"
                            LEFT JOIN stock_picking sp ON sml.picking_id = sp."id" 
                        WHERE
                            sl."usage" = 'customer' 
                        GROUP BY
                            sml.product_id,
                            sp.origin 
                        ) 
                    SELECT
                        rp."name" AS customer,
                        pt."name" [ 'en_US' ] AS product, 
                        sol.product_uom_qty AS quantity,
                        sol.qty_delivered AS delivered,
                        COALESCE(rps.return_demand, 0),
	                    COALESCE(rps.returned, 0),
                        sol.purchase_price AS COST,
                        sol.price_unit,
                        sol.margin,
                        TO_CHAR( sml."date", 'dd/mm/yyyy' ) 
                    FROM
                        stock_move_line sml
                        LEFT JOIN product_product pp ON sml.product_id = pp."id"
                        LEFT JOIN product_template pt ON pp.product_tmpl_id = pt."id"
                        LEFT JOIN stock_location sl ON sml.location_dest_id = sl."id"
                        LEFT JOIN stock_picking sp ON sml.picking_id = sp."id"
                        INNER JOIN sale_order so ON sp.sale_id = so."id"
                        LEFT JOIN res_partner rp ON so.partner_id = rp."id"
                        LEFT JOIN sale_order_line sol ON so."id" = sol.order_id 
                        AND sml.product_id = sol.product_id
                        LEFT JOIN return_products rps ON sml.reference = rps.reference 
                        AND sml.product_id = rps.product_id 
                    WHERE
                        sml."state" = 'done' 
                        AND sl."usage" = 'customer'"""

        if self.month and self.year:
            query += f"""  
                        AND EXTRACT(MONTH FROM sml."date") = {self.month}
	                    AND EXTRACT(YEAR FROM sml."date") = {self.year}"""
        query += f"""
                    ORDER BY
                        sml."date" DESC"""

        self.env.cr.execute(query)
        data = self.env.cr.fetchall()

        data = {
            'data': data,
            'month': self.month,
            'year': self.year
        }
        # for value in data['data']:
        #     print(value)

        return self.env.ref('om_hospital.report_excel_xlsx').report_action(self, data=data)

    def action_print_excel(self):
        query = f"""WITH sodu_dauky AS (
                        SELECT 
                            sml.product_id,
                            SUM ( CASE 
                                                WHEN sl.USAGE != 'internal' AND sl2.USAGE = 'internal' THEN sml.qty_done 
                                                WHEN sl.USAGE = 'internal' AND sl2.USAGE != 'internal' THEN -sml.qty_done
                                        ELSE 0 END ) AS sodudauky 
                        FROM
                            stock_move_line sml  
                            LEFT JOIN stock_location sl ON sml.location_id = sl."id" 
                            LEFT JOIN stock_location sl2 ON sml.location_dest_id = sl2."id"
                        WHERE
                            sml."state" = 'done' AND sml."date" < '{self.date_from}'
                        GROUP BY  
                            sml.product_id
                    ),
                    nhap_xuat AS (
                        SELECT
                            sml."id",
                            ( CASE 
                                    WHEN sl."usage" != 'internal' AND sl2."usage" = 'internal' THEN sml.qty_done 
                                    WHEN sl."usage" = 'internal' AND sl2."usage" != 'internal' THEN -sml.qty_done 
                                    ELSE 0 END 
                            ) AS nhapxuat 							
                        FROM
                            stock_move_line sml 
                            LEFT JOIN stock_location sl ON sml.location_id = sl."id"
                            LEFT JOIN stock_location sl2 ON sml.location_dest_id = sl2."id"   
                        GROUP BY 
                            sl."usage",
                            sl2."usage",
                            sml."id"
                    )
                    SELECT 
                        TO_CHAR(sml."date", 'dd/mm/yyyy'),
                        pt."name" [ 'en_US' ] AS tensp,
                        nx.nhapxuat,
                        COALESCE(
                            sddk.sodudauky + SUM(nx.nhapxuat) 
                            OVER (PARTITION BY sml.product_id ORDER BY sml."date" ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING),
                            sddk.sodudauky
                        ) AS sodudauky,
                        nx.nhapxuat + COALESCE(
                            sddk.sodudauky + SUM(nx.nhapxuat) 
                            OVER (PARTITION BY sml.product_id ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING),
                            sddk.sodudauky
                        ) AS luyke 
                    FROM
                        stock_move_line sml
                        LEFT JOIN product_product pp ON sml.product_id = pp."id"
                        LEFT JOIN product_template pt ON pp.product_tmpl_id = pt."id" 
                        LEFT JOIN sodu_dauky sddk ON sddk.product_id = sml.product_id
                        LEFT JOIN nhap_xuat nx ON nx."id" = sml."id"
                    WHERE
                        sml."state" = 'done' 
                        AND ( sml.date BETWEEN '{self.date_from}' AND '{self.date_to}' ) 
                        AND sml.qty_done != 0
                    ORDER BY
                        pt."name",
                        sml."date" 	"""

        self.env.cr.execute(query)
        data = self.env.cr.fetchall()

        data = {
            'data': data,
            'from': self.date_from,
            'to': self.date_to
        }

        return self.env.ref('om_hospital.xlsx_report_excel').report_action(self, data=data)

