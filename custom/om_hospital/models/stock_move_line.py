# -*- coding: utf-8 -*-
from odoo import api, fields, models


class StockMoveLine(models.Model):
    _inherit = 'stock.move.line'

    nhap_xuat = fields.Float('Nhập/Xuất', default=0.0, digits='Product Unit of Measure', copy=False,
                             compute='_compute_nhap_xuat', store=True)

    @api.depends('qty_done', 'location_usage', 'location_dest_usage')
    def _compute_nhap_xuat(self):
        for line in self:
            if line.location_usage in ('internal', 'transit') and line.location_dest_usage not in ('internal', 'transit'):
                line.nhap_xuat = -line.qty_done
            elif line.location_usage not in ('internal', 'transit') and line.location_dest_usage in ('internal', 'transit'):
                line.nhap_xuat = line.qty_done
