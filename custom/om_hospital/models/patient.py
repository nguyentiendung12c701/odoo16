# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import datetime
from odoo.exceptions import ValidationError
import random


class HospitalPatient(models.Model):
    _name = 'hospital.patient'
    _inherit = ["mail.thread", "mail.activity.mixin"]
    _description = 'Hospital Patient'
    _order = 'name asc'

    @api.model
    def default_get(self, fields):
        res = super(HospitalPatient, self).default_get(fields)
        res['age'] = random.randint(20, 40)
        note_list = [
            'Make appointments after 6pm',
            'Only woman doctor',
            'Only man doctor',
            'Always prepare medicine',
            'End stage cancer',
            'Pollen allergy',
            'It is forbidden to bring animals'
        ]
        res['note'] = note_list[random.randint(0, 6)]
        res['gender'] = random.choice(["male", "female", "other"])
        return res

    name = fields.Char(string='Name', required=True, tracking=True)
    reference = fields.Char(string="Order Reference", required=True, copy=False, readonly=True,
                            default=lambda self: _('New'))
    age = fields.Integer(string='Age', tracking=True)
    gender = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female'),
        ('other', 'Other')
    ], required=True, default='male', tracking=True)
    note = fields.Text(string='Description', tracking=True)
    state = fields.Selection([('draft', 'Draft'), ('confirm', 'Confirmed'), ('done', 'Done'),
                              ('cancel', 'Cancelled')], default='draft', string='Status', tracking=True)
    responsible_id = fields.Many2one('res.partner', string="Responsible")
    appointment_count = fields.Integer(string='Appointment Count', compute='_compute_appointment_count')
    year = fields.Integer(string='Year of Birth', compute='_compute_year')
    image = fields.Binary(string='Patient Image')
    appointment_ids = fields.One2many('hospital.appointment', 'patient_id', string='Appointments')

    @api.depends('age')
    def _compute_year(self):
        for record in self:
            if record.age:
                record.year = datetime.now().year - record.age
            else:
                record.year = 0

    def _compute_appointment_count(self):
        appointment_count = self.env['hospital.appointment'].search_count([('patient_id', '=', self.id)])
        self.appointment_count = appointment_count

    def action_confirm(self):
        self.state = 'confirm'

    def action_done(self):
        self.state = 'done'

    def action_draft(self):
        self.state = 'draft'

    def action_cancel(self):
        self.state = 'cancel'

    @api.model
    def create(self, vals):
        if not vals.get('note'):
            vals['note'] = 'New patient'
        if vals.get('reference', _('New') == _('New')):
            vals['reference'] = self.env['ir.sequence'].next_by_code('hospital.patient') or _('New')
        res = super(HospitalPatient, self).create(vals)
        return res

    @api.constrains('name')
    def check_name(self):
        for rec in self:
            patient = self.env['hospital.patient'].search([('name', '=', rec.name), ('id', '!=', rec.id)])
            if patient:
                raise ValidationError(_('Name %s already exists' % rec.name))

    @api.constrains('age')
    def check_age(self):
        for rec in self:
            if rec.age == 0:
                raise ValidationError(_('Age cannot be 0...!'))

    def name_get(self):
        result = []
        for rec in self:
            if not self.env.context.get('hide_code'):
                name = f'[{rec.reference}] {rec.name}'
            else:
                name = rec.name
            result.append((rec.id, name))
        return result

    def action_open_appointments(self):
        return {
            'type': 'ir.actions.act_window',
            'name': 'Appointments',
            'res_model': 'hospital.appointment',
            'domain': [('patient_id', '=', self.id)],
            'context': {'default_patient_id': self.id},
            'view_mode': 'tree,form',
            'target': 'current'
        }

    def _get_report_base_filename(self):
        return self.name
