odoo.define('om_hospital.NewPatientForm', function(require) {
"use strict";

var publicWidget = require("web.public.widget");

publicWidget.registry.NewPatientForm = publicWidget.Widget.extend({
    selector: '#new_patient_creation',
    events: {
        'submit': '_onSubmitButton',
    },

    _onSubmitButton: (e) => {
        document.querySelector('#form_view_warning_msg').classList.remove('d-none')
//        e.preventDefault()
    }
})
})